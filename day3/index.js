const express = require("express");
const exphbs = require("express-handlebars");
const path = require("path");

const app = express();
const port = process.env.PORT || 3000;

app.engine(
  "handlebars",
  exphbs({
    runtimeOptions: {
      allowProtoPropertiesByDefault: true,
      allowProtoMethodsByDefault: true,
    },
  })
);
app.set("view engine", "handlebars");
app.set("views", "./views");

app.use(express.static("public"));

app.get("/", async (req, res) => {
  res.render("home");
});

app.listen(port, () => {
  console.log(`Mongoose example running at http://localhost:${port}`);
});
