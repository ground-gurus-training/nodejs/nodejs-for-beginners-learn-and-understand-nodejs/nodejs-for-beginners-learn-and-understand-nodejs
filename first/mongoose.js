const mongoose = require("mongoose");

mongoose.connect(
  "mongodb+srv://cluster0.o5urb.mongodb.net/day3?retryWrites=true&w=majority",
  {
    user: "yourUsername",
    pass: "yourPassword",
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connect("mongodb://localhost:27017/day3", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
